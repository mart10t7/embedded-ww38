import socket
import sys
import struct
from gpiozero import CPUTemperature
import spidev # To communicate with SPI devices
from numpy import interp    # To scale values
from time import sleep  # To add delay
import RPi.GPIO as GPIO # To use GPIO pins

spi = spidev.SpiDev() # Created an object
spi.open(0,0)   
# Initializing LED pin as OUTPUT pin
led_pin = 20
GPIO.setmode(GPIO.BCM)
GPIO.setup(led_pin, GPIO.OUT)
# Creating a PWM channel at 100Hz frequency
pwm = GPIO.PWM(led_pin, 100)
pwm.start(0) 
cputemp = CPUTemperature()
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('192.168.33.6', 10000)
print('connecting to {} port {}'.format(*server_address))
sock.connect(server_address)
while True:


		# Send data
	message =  struct.pack('!d',cputemp.temperature)
	sock.sendall(message)
	
		# Look for the response
	amount_received = 0
	amount_expected = len(message)
	while amount_received < amount_expected:
		data = sock.recv(16)
		amount_received += len(data)
	num = struct.unpack('!d',data)
	pwmchange = num[0]
	pwm.ChangeDutyCycle(pwmchange)
	sleep(0.5)


