import RPi.GPIO as GPIO # GPIO library
from time import sleep # Time library
GPIO.setwarnings(False) #Set warnings off
GPIO.setmode(GPIO.BCM) #Setting the pins mode to Broadcom SOC Channel
#Set Button and LED pins
Button = 23
LED = 24
#Setup Button and LED
GPIO.setup(Button,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(LED,GPIO.OUT)
flag = 0
yes_no = ""
PWM=GPIO.PWM(LED,50) # Setup PWM to GPIO pin
while True:
 varmenu = input("Enter 'ssh' or 'button' ") # User choice of input type
 if varmenu == "button":
      while varmenu == "button":
        button_state = GPIO.input(Button) # Button input
        if button_state == 0:
            PWM.ChangeFrequency(1)
            if flag == 0:
                flag=1
                PWM.start(50) # Start LED with 50Hz Frequency
                sleep(.5)
            else:
                if flag == 1:
                    flag = 2
                    PWM.start(25) 
                    sleep(.5)
                else:
                    if flag == 2:
                        flag = 0
                        PWM.stop() # Stop LED
                        sleep(.5)
                        yes_no = input("Do you want to continue with button? yes/no: ")
                        if yes_no == "no":
                            varmenu = ""
 if varmenu == "ssh":
      while varmenu == "ssh":
        frequency_button = input("Enter 1, 2 or 3 for LED control ")
        if frequency_button == "1":
            PWM.ChangeFrequency(1)
            print("1")
            PWM.start(50)
            sleep(.5)
        else:
            if frequency_button == "2":
                PWM.ChangeFrequency(1)
                PWM.start(25)
                print("2")
                sleep(.5)
            else:
                if frequency_button == "3":
                    PWM.stop()
                    print("3")
                    varmenu = ""
                    sleep(.5)
        
