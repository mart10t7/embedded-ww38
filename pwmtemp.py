# Importing modules
import spidev # To communicate with SPI devices
from numpy import interp    # To scale values
from time import sleep  # To add delay
import RPi.GPIO as GPIO # To use GPIO pins
from gpiozero import CPUTemperature

cputemp = CPUTemperature()
# Start SPI connection
spi = spidev.SpiDev() # Created an object
spi.open(0,0)   
# Initializing LED pin as OUTPUT pin
led_pin = 20
GPIO.setmode(GPIO.BCM)
GPIO.setup(led_pin, GPIO.OUT)
# Creating a PWM channel at 100Hz frequency
pwm = GPIO.PWM(led_pin, 100)
pwm.start(0) 

while True:
	temperature = cputemp.temperature
	print(temperature)
	if temperature>50:
		pwm.ChangeDutyCycle(98)
		sleep(0.1)

